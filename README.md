# A Core Decomposition For High-Resolution Temporal Networks

This repository contains the source code for our KDD 2024 submission ```A Core Decomposition For High-Resolution Temporal Networks.```

## Compilation

### (k,Delta)-core decomposition
In order to run the code of the (k,Delta)-core decomposition, run the following in folder ```tgkdcore```.

```
mkdir release
cd release
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

After compilation running `tgkdcore` shows options and help.
Parameter -d=3600 sets delta, -t=0 for exact computation, -t=1 for approximation, -e=0.2 for epsilon.
The results will be written to a file in the data set folder.

For example, run the following in folder ```tgkdcore/release``` for exact computation:
```
./tgkdcore ../../datasets/highschool.txt -t=0 -d=3600
```

Or run the following for approximation computation:
```
./tgkdcore ../../datasets/highschool.txt -t=1 -e=0.1 -d=3600
```


### Delta-components and component search
In order to run the code of the Delta-connected components and component search, run the 
following in folder ```deltacc```. The results will be written to a file in the data set folder.


```
mkdir release
cd release
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

After compilation running `tgdcc` shows options and help.

For example, run the following in folder ```deltacc/release``` for the Delta-connected components:
```
./tgdcc ../../datasets/highschool.txt -t=0 -d=3600
```

Or run the following for the Delta-connected component search for query node 1:
```
./tgdcc ../../datasets/highschool.txt -t=1 -q=1 -d=3600
```

## Data Sets

Run command `unzip datasets.zip`. 
The folder `datasets` then contains all data sets but StackOverflow due to its large size.

The format for the temporal graphs is the following:
First line states number of nodes. Each following line of the format `u v t` represents a temporal edge (u,v,t) from node u to v at time t. Nodes are indexed from 0 to number of nodes - 1. 

In the case of `Twitter`, the fourth column shows if the interaction is a claim (0) or fact-checking (1).

Please refer to the paper for references of the data sets.

