#ifndef TGDC_TGS_H
#define TGDC_TGS_H

#include <cstdint>
#include <tuple>
#include <string>
#include <vector>
#include <set>

namespace tglib {
    using NodeId = int32_t;
    using EdgeId = int64_t;
    using Time = int64_t;
}

struct MyTemporalEdge {
    tglib::NodeId u;
    tglib::NodeId v;
    tglib::Time t;
    tglib::EdgeId id;
};

inline MyTemporalEdge getLowerBoundEdge(tglib::Time t) {
    return MyTemporalEdge{0,0,t,0};
}

inline MyTemporalEdge getUpperBoundEdge(tglib::Time t) {
    return MyTemporalEdge{INT32_MAX,INT32_MAX,t,INT64_MAX};
}

inline bool operator<(const MyTemporalEdge &e1, const MyTemporalEdge &e2) {
    return std::tie(e1.t, e1.u, e1.v, e1.id) < std::tie(e2.t, e2.u, e2.v, e2.id);
}

inline bool operator==(const MyTemporalEdge &e1, const MyTemporalEdge &e2) {
    return e1.u == e2.u && e1.v == e2.v && e1.t == e2.t;
}


struct TGS {
    TGS(uint numberOfNodes, std::vector<MyTemporalEdge> const &edges);

    std::vector<std::set<MyTemporalEdge>> nodes;
    std::vector<MyTemporalEdge> all_edges;
    uint numberOfEdges{};

    [[nodiscard]] uint getNumberOfDeltaNeighbors(tglib::NodeId, tglib::Time t, tglib::Time delta) const;

    void remove(tglib::EdgeId);
};



#endif //TGDC_TGS_H
