#ifndef TGDC_KDCORESETBASED_H
#define TGDC_KDCORESETBASED_H

#include "TGS.h"

std::vector<uint> computeDeltaKCoreSB_original(TGS &tgs, tglib::Time delta);

std::vector<uint> computeDeltaKCoreApprox(TGS &tgs, tglib::Time delta, double epsilon);


#endif //TGDC_KDCORESETBASED_H
