#include <ranges>
#include <sstream>
#include <iostream>
#include <chrono>
#include <fstream>
#include "kdCore.h"

using namespace std;
using namespace tglib;

std::vector<Time> split_string1(const std::string& s, char delim) {
    std::vector<Time> result;
    std::vector<string> strings;
    std::stringstream ss(s);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, delim);
        if (!substr.empty()) {
            try {
                stol(substr);
            }
            catch(...) {
                continue;
            }
            result.push_back(stol(substr));
        }
    }
    return result;
}


TGS load(std::string const &filename) {
    std::ifstream fs;
    fs.open(filename);
    auto opened_file = fs.is_open();
    if (!opened_file) {
        throw std::runtime_error("Could not open file " + filename);
    }

    std::string line;
    std::vector<MyTemporalEdge> edges;
    NodeId mnid = 0;
    while (getline(fs, line)) {
        auto l = split_string1(line, ' ');
        if (l.size() < 3) continue;
        NodeId u = l[0];
        NodeId v = l[1];
        if (v > mnid) mnid = v;
        Time t = l[2];
        edges.push_back(MyTemporalEdge{u, v, t, 0});
    }
    fs.close();

    cout << "Loaded: " << filename << endl;
    cout << "#nodes: " << mnid+1 << endl;
    cout << "#edges: " << edges.size() << endl;

    TGS tgs(mnid+1, edges);
    return tgs;
}


class Timer {
public:
    void start() {
        timePoint = std::chrono::high_resolution_clock::now();
    }

    double stop() {
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - timePoint;
        return elapsed.count();
    }

    void stopAndPrintTime() {
        auto t = stop();
        cout << "Elapsed: " << t << std::endl;
    }


private:
    std::chrono::high_resolution_clock::time_point timePoint{};
};

enum Task {
    DeltaKCores,
    RunApproxSingle,
};

struct Params {
    std::string dataset_path;
    tglib::Time delta = 1000;
    double epsilon = 0.01;
    Task task = DeltaKCores;
};


void save_kcores(vector<uint> const &kcores, string const &filename) {
    ofstream f(filename);
    for (auto &e : kcores) {
        f << e << endl;
    }
    f.close();
}

const string error_str = "Usage: tgkdcore file [-t=0 exact, 1 approx] -d=[delta] -e=[epsilon]";

int main(int argc, char* argv[]) {
    vector<string> args;
    for (int i = 1; i < argc; ++i) args.emplace_back(argv[i]);

    Params params;
    if (args.size() < 3) {
        cout << error_str << endl;
        exit(1);
    }

    params.dataset_path = args[0];

    for (size_t i = 1; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-d=") {
            string valstr = next.substr(3);
            params.delta = stoul(valstr);
        } else if (next.substr(0, 3) == "-t=") {
            string valstr = next.substr(3);
            params.task = static_cast<Task>(stoi(valstr));
        } else if (next.substr(0, 3) == "-e=") {
            string valstr = next.substr(3);
            params.epsilon = stod(valstr);
        }  else {
            cout << error_str << endl;
            exit(1);
        }
    }

    switch (params.task) {

        case DeltaKCores: {
            cout << "Computing (k,d)-cores" << endl;
            TGS tgs = load(params.dataset_path);
            Timer timer;
            vector<uint> cores;
            timer.start();
            cores = computeDeltaKCoreSB_original(tgs, params.delta);
            timer.stopAndPrintTime();
            save_kcores(cores, params.dataset_path+"_d" + to_string(params.delta) + ".kdcores");
            break;
        }

        case RunApproxSingle : {
            cout << "Computing (k,d)-cores Approximation" << endl;
            TGS tgs = load(params.dataset_path);
            Timer timer;
            vector<uint> cores;
            timer.start();
            auto cores_approx = computeDeltaKCoreApprox(tgs, params.delta, params.epsilon);
            timer.stopAndPrintTime();
            save_kcores(cores_approx, params.dataset_path+"_d" + to_string(params.delta)+"_e" + to_string(params.epsilon) + ".approx");
            break;
        }
        default : {
            cout << error_str << endl;
            exit(1);
        }
    }

    return 0;
}
