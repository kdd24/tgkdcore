#include "TGS.h"

using namespace std;
using namespace tglib;

uint TGS::getNumberOfDeltaNeighbors(tglib::NodeId nid, tglib::Time t, tglib::Time delta) const {
    auto l = nodes[nid].lower_bound(getLowerBoundEdge(max(0l, t - delta)));
    auto u = nodes[nid].upper_bound(getUpperBoundEdge(t + delta )); //+1
    return std::distance(l, u);
}


void TGS::remove(tglib::EdgeId eid) {
    auto const &e = all_edges[eid];
    MyTemporalEdge tge{e.u, e.v, e.t, e.id};
    nodes[e.u].erase(tge);
    nodes[e.v].erase(tge);
}

TGS::TGS(uint numberOfNodes, vector<MyTemporalEdge> const &edges) {
    numberOfEdges = edges.size();
    nodes.resize(numberOfNodes);
    EdgeId eid = 0;
    for (MyTemporalEdge e : edges) {
        e.id = eid++;
        all_edges.push_back(e);
        nodes[e.u].insert(e);
        nodes[e.v].insert(e);
    }
}

