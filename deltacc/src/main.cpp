#include <ranges>

#include "algorithms/kdCoreSetBased.h"
#include "algorithms/DeltaConnected.h"
#include "algorithms/StaticCommunitySearch.h"
#include "algorithms/DeltaComponentSearch.h"
#include <iostream>
#include <chrono>
#include <fstream>

using namespace std;
using namespace tglib;

void show_help();

enum Task {
    DeltaConnectedComponents,
    DeltaCommunitySearchKDcore,
    StaticCommunitySearch,
};

struct Params {
    std::string dataset_path;
    tglib::Time delta = 60;
    tglib::Time q = 0;
    Task task{};
    bool parseArgs(std::vector<std::string> args) {
        if (args.size() < 3) {
            return false;
        }
        for (auto & arg : args) {
            cout << arg << " ";
        }
        cout << endl;

        try {
            dataset_path = args[0];
        } catch (...) {
            return false;
        }

        for (size_t i = 1; i < args.size(); ++i) {
            string next = args[i];
            if (next.length() < 4) return false;
            if (next.substr(0, 3) == "-d=") {
                string valstr = next.substr(3);
                delta = stoul(valstr);
                cout << "set delta " << delta << endl;
            } else if (next.substr(0, 3) == "-q=") {
                string valstr = next.substr(3);
                q = stoi(valstr);
                cout << "set q " << q << endl;
            } else if (next.substr(0, 3) == "-t=") {
                string valstr = next.substr(3);
                task = static_cast<Task>(stoi(valstr));
                cout << "set task " << task << endl;
            } else {
                return false;
            }
        }
        return true;
    }
};


void show_help() {
    std::cout << "tgdcmemeff dataset [-t=mode] [-d=delta] [-q=query node]" << std::endl;
    cout << "modes:\n";
    cout << DeltaConnectedComponents << "\tDeltaConnectedComponents" << endl;
    cout << DeltaCommunitySearchKDcore << "\tDeltaCommunitySearch" << endl;
    cout << StaticCommunitySearch << "\tStaticCommunitySearch" << endl;
}

class Timer {
public:
    void start() {
        timePoint = std::chrono::high_resolution_clock::now();
    }

    double stop() {
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - timePoint;
        return elapsed.count();
    }

    void stopAndPrintTime() {
        auto t = stop();
        std::cout << "Elapsed: " << t << std::endl;
    }

private:
    std::chrono::high_resolution_clock::time_point timePoint{};
};


void writeVectorToFile(const std::string &filename, const std::vector<uint>& data) {
    std::ofstream fs;
    fs.open(filename);
    if (!fs.is_open()) {
        std::cout << "Could not write data to " << filename << std::endl;
        return;
    }
    for (auto &d : data)
        fs << d << std::endl;

    fs.close();
}


template <typename T>
void writeVectorToFileT(const std::string &filename, const std::vector<T>& data) {
    std::ofstream fs;
    fs.open(filename);
    if (!fs.is_open()) {
        std::cout << "Could not write data to " << filename << std::endl;
        return;
    }
    for (auto &d : data)
        fs << d << std::endl;

    fs.close();
}


int main(int argc, char* argv[]) {
    vector<string> args;
    for (int i = 1; i < argc; ++i) args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args)) {
        show_help();
        return 1;
    }

    Timer timer;

    switch (params.task) {

        case DeltaConnectedComponents: {
            cout << "Computing connected components" << endl;
            TGS tgs = load(params.dataset_path);
            timer.start();
            auto components = computeDCC_alternative(tgs, params.delta);
            timer.stopAndPrintTime();
            cout << "number of components: " << components.size() << endl;

            vector<uint> data(tgs.numberOfEdges, 0);
            for (auto &c : components) {
                for (auto &e : c.second) {
                    data[e.id] = c.first;
                }
            }
            auto filename = params.dataset_path+"_d" + to_string(params.delta) + ".dccs";
            writeVectorToFile(filename, data);
            cout << "write results to " << filename << endl;

            break;
        }

        case DeltaCommunitySearchKDcore: {
            cout << "Computing DeltaCommunitySearch" << endl;
            TGS tgs = load(params.dataset_path);
            auto q = params.q;

            cout << "Computing DeltaCommunitySearch (with kd-core)" << endl;
            Timer timer;
            timer.start();
            auto dcc = searchDeltaConnectedComponentSearch({tgs.all_edges.begin(), tgs.all_edges.end()}, q, params.delta);
            timer.stopAndPrintTime();

            set<tglib::NodeId> nids;
            set<Time> times;
            vector<string> result;
            for (auto &e : dcc.second) {
                nids.insert(e.u);
                nids.insert(e.v);
                times.insert(e.t);
                auto s = to_string(e.u) + " " + to_string(e.v) + " " + to_string(e.t);
                result.push_back(s);
            }

            auto filename = params.dataset_path + "_d" + to_string(params.delta)  + "_q" + to_string(q) + ".cs";
            writeVectorToFileT(filename, result);
            cout << filename << endl;

            cout << "Q: " << to_string(q) << endl;
            cout << "#nodes: " << nids.size() << endl;
            cout << "#edges: " << dcc.second.size() << endl;
            cout << "times:  " << times.size() << endl;
            break;
        }

        case StaticCommunitySearch: {
            cout << "Computing Static Community" << endl;
            TGS tgs = load(params.dataset_path);
            auto q = params.q;

            cout << "Computing Static CommunitySearch" << endl;
            Timer timer;

            timer.start();
            auto results = compute_static_community(tgs, q);
            timer.stopAndPrintTime();

            set<Time> times;
            set<MyTemporalEdge> edges;
            vector<string> result;

            for (auto &e : tgs.all_edges) {
                if (results.second.contains(e.u) && results.second.contains(e.v)) {
                    edges.insert(e);
                    times.insert(e.t);

                    auto s = to_string(e.u) + " " + to_string(e.v) + " " + to_string(e.t);
                    result.push_back(s);
                }
            }

            auto filename = params.dataset_path + "_d" + to_string(params.delta)  + "_q" + to_string(q) + ".scs";
            writeVectorToFileT(filename, result);
            cout << filename << endl;

            cout << "Q: " << to_string(q) << endl;
            cout << "#nodes: " << results.second.size() << endl;
            cout << "#edges: " << edges.size() << endl;
            cout << "times:  " << times.size() << endl;
            break;
        }

        default: {
            show_help();
        }
    }

    return 0;
}
