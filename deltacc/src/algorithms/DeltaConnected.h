#ifndef TGDC_DELTACONNECTED_H
#define TGDC_DELTACONNECTED_H

#include "../tg/TGS.h"

std::map<uint,std::set<MyTemporalEdge>> computeDCC_alternative(TGS const &tgs, long delta);

std::map<uint,std::vector<MyTemporalEdge>> computeDCC_alternative(std::set<MyTemporalEdge> const &edges, long delta);

#endif //TGDC_DELTACONNECTED_H
