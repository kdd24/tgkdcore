#include "kdCoreSetBased.h"


using namespace std;
using namespace tglib;


vector<uint> computeDeltaKCoreSB_original(TGS tgs, Time delta){

    auto num_edges = tgs.numberOfEdges;

    vector<long> a_u(num_edges, 0);
    vector<long> a_v(num_edges, 0);
    vector<uint> d(num_edges, 0);

    vector<MyTemporalEdge> sorted_edges(num_edges);
    long md = 0;

    for (EdgeId eid = 0; eid < num_edges; ++eid) {
        auto &e = tgs.all_edges[eid];
        a_u[eid] = tgs.getNumberOfDeltaNeighbors(e.u, e.t, delta);
        a_v[eid] = tgs.getNumberOfDeltaNeighbors(e.v, e.t, delta);
        d[eid] = min(a_u[eid], a_v[eid]);

        if (md < d[eid]) md = d[eid];
    }

    vector<uint> bins(md+1, 0);
    for (auto eid = 0; eid < num_edges; ++eid) {
        bins[d[eid]] += 1;
    }
    uint start = 0, num;
    for (uint deg = 0; deg <= md; ++deg) {
        num = bins[deg];
        bins[deg] = start;
        start += num;
    }
    vector<uint> pos(num_edges, 0);
    for (auto eid = 0; eid < num_edges; ++eid) {
        pos[eid] = bins[d[eid]];
        sorted_edges[pos[eid]] = tgs.all_edges[eid];
        bins[d[eid]]++;
    }
    for (auto i = md; i > 0; i--) {
        bins[i] = bins[i-1];
    }
    bins[0] = 0;

    uint old_d{}, du{}, pu{}, pw{};
    MyTemporalEdge u{}, w{};

    for (EdgeId i = 0; i < num_edges; ++i) {
        auto &e = sorted_edges[i];

        auto it = tgs.nodes[e.u].lower_bound(getLowerBoundEdge(e.t-delta));
        while ((it != tgs.nodes[e.u].end()) && ((*it).t <= e.t + delta)) {

            if (d[(*it).id] > d[e.id]) {
                old_d = d[(*it).id];
                if ((*it).u == e.u)
                    a_u[(*it).id]--;
                else
                    a_v[(*it).id]--;

                d[(*it).id] = min(a_u[(*it).id], a_v[(*it).id]);
                if (d[(*it).id] < old_d) {
                    du = old_d;
                    pu = pos[(*it).id];
                    pw = bins[du];
                    u = sorted_edges[pu];
                    w = sorted_edges[pw];
                    if (u.id != w.id) {
                        pos[u.id] = pw;
                        pos[w.id] = pu;
                        sorted_edges[pu] = w;
                        sorted_edges[pw] = u;
                    }
                    bins[du]++;
                }
            }
            ++it;
        }

        it = tgs.nodes[e.v].lower_bound(getLowerBoundEdge(e.t-delta));
        while (it != tgs.nodes[e.v].end() && ((*it).t <= e.t + delta)) {

            if (d[(*it).id] > d[e.id]) {
                old_d = d[(*it).id];
                if ((*it).u == e.v)
                    a_u[(*it).id]--;
                else
                    a_v[(*it).id]--;

                d[(*it).id] = min(a_u[(*it).id], a_v[(*it).id]);
                if (d[(*it).id] < old_d) {
                    du = old_d;
                    pu = pos[(*it).id];
                    pw = bins[du];
                    u = sorted_edges[pu];
                    w = sorted_edges[pw];
                    if (u.id != w.id) {
                        pos[u.id] = pw;
                        pos[w.id] = pu;
                        sorted_edges[pu] = w;
                        sorted_edges[pw] = u;
                    }
                    bins[du]++;
                }
            }
            ++it;
        }
        tgs.remove(e.id);
    }

    return d;
}

map<uint, vector<MyTemporalEdge>> cores_to_substreams(TGS const &tgs, vector<uint> const &cores) {
    map<uint, vector<MyTemporalEdge>> result;
    EdgeId eid = 0;
    for (auto &c : cores) {
        result[c].push_back(tgs.all_edges[eid++]);
    }
    return result;
}
