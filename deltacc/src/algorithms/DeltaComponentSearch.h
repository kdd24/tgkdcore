#ifndef TGDC_DELTACOMPONENTSEARCH_H
#define TGDC_DELTACOMPONENTSEARCH_H

#include "kdCoreSetBased.h"

struct EDS {
    std::map<uint, std::vector<MyTemporalEdge>> core_numbers;
    std::set<tglib::NodeId> nodes;
    std::set<MyTemporalEdge> edges;
    std::map<tglib::NodeId, std::set<std::pair<tglib::Time, tglib::EdgeId>>> incidentTimes;
    std::map<uint, std::vector<MyTemporalEdge>>::reverse_iterator rit;

    tglib::Time delta;

    [[nodiscard]] bool contains(tglib::NodeId q) const{
        return nodes.contains(q);
    }

    void addShell();

    void init(std::map<uint, std::vector<MyTemporalEdge>> const &cores);

    bool addNextShell();

};

std::pair<uint, std::vector<MyTemporalEdge>> searchDeltaConnectedComponentSearch(const std::set<MyTemporalEdge>& edges, tglib::NodeId q, tglib::Time delta);


#endif //TGDC_DELTACOMPONENTSEARCH_H
