#include <list>
#include "DeltaConnected.h"
#include "../tg/StaticRepresentation.h"

using namespace std;
using namespace tglib;


void non_recursive_dfs(SG &g, NodeId u, set<EdgeId> &edges) {
    list<NodeId> nodes_to_visit;
    nodes_to_visit.push_back(u);
    while(!nodes_to_visit.empty()) {
        auto currentnode = nodes_to_visit.front();
        nodes_to_visit.pop_front();
        for (auto &n : g.nodes[currentnode].adjacent) {
            if (!n.waitingEdge) edges.insert(n.e);
            auto &v = g.nodes[n.v];
            if (v.visited) continue;
            v.visited = true;
            nodes_to_visit.push_front(v.id);
        }
    }
}

std::map<uint,std::set<MyTemporalEdge>> computeDCC_alternative(TGS const &tgs, long delta) {

    auto sg = transformToSG(tgs, delta);

    uint cc = 0;
    std::map<uint, std::set<MyTemporalEdge>> result;
    for (auto &n : sg.nodes) {
        if (!n.visited) {
            n.visited = true;
            set<EdgeId> edges;
            non_recursive_dfs(sg, n.id, edges);

            for (auto &eid : edges) {
                result[cc].insert(tgs.all_edges[eid]);
            }
            cc++;
        }
    }

    return result;
}

std::map<uint,std::vector<MyTemporalEdge>> computeDCC_alternative(set<MyTemporalEdge> const &edges, long delta) {

    auto edges_vec = vector<MyTemporalEdge>(edges.begin(), edges.end());
    for (EdgeId i = 0; i < edges_vec.size(); ++i) {
        edges_vec[i].id = i;
    }
    auto sg = transformToSG(edges_vec, delta);

    uint cc = 0;
    std::map<uint, std::vector<MyTemporalEdge>> result;
    for (auto &n : sg.nodes) {
        if (!n.visited) {
            n.visited = true;
            set<EdgeId> result_edges;
            non_recursive_dfs(sg, n.id, result_edges);

            for (auto &eid : result_edges) {
                result[cc].push_back(edges_vec[eid]);
            }
            cc++;
        }
    }

    return result;
}
