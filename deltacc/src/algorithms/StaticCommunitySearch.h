#ifndef TGDC_STATICCOMMUNITYSEARCH_H
#define TGDC_STATICCOMMUNITYSEARCH_H

#include <vector>
#include "../tg/TGS.h"

std::pair<uint, std::set<tglib::NodeId>> compute_static_community(TGS const &tgs, tglib::NodeId q);

#endif //TGDC_STATICCOMMUNITYSEARCH_H
