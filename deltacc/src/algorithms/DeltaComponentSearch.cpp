#include "DeltaComponentSearch.h"
#include "DeltaConnected.h"

using namespace std;
using namespace tglib;


pair<uint, vector<MyTemporalEdge>> searchDeltaConnectedComponentSearch(const std::set<MyTemporalEdge> &edges, NodeId q, Time delta) {
    if (edges.empty()) return {};

    auto [num_nids, nedges] = reindex_edges({edges.begin(), edges.end()});
    std::set<MyTemporalEdge> tes;
    map<NodeId, NodeId> nmap;
    map<NodeId, NodeId> rnmap;
    for (auto &e : nedges){
        tes.insert(e);
        nmap[e.u] = e.org_u;
        nmap[e.v] = e.org_v;
        rnmap[e.org_u] = e.u;
        rnmap[e.org_v] = e.v;
    }

    TGS tgs(num_nids, tes);
    auto tgs2 = tgs;

    for (auto &e : tgs.all_edges) {
        e.org_u = nmap[e.u];
        e.org_v = nmap[e.v];
    }

    auto cores = computeDeltaKCoreSB_original(tgs2, delta);
    auto substreams = cores_to_substreams(tgs, cores);

    EDS eds;
    eds.init(substreams);

    // iterate over (k,delta)-cores, starting with highest k
    do {
        if (!eds.contains(rnmap[q])) continue;

        std::vector<MyTemporalEdge> result = {eds.edges.begin(), eds.edges.end()};
        for (auto &e : result) {
            e.u = nmap[e.u];
            e.v = nmap[e.v];
        }

        // compute decomposition of (k,delta)-core
        auto dcc = computeDCC_alternative({result.begin(), result.end()}, delta);

        for (auto & subgraph_of_kd_core : dcc) {
            // does subgraph contain the query nodes?
            set<NodeId> nodeids;
            for (auto &e : subgraph_of_kd_core.second) {
                nodeids.insert(e.u);
                nodeids.insert(e.v);
            }

            if (nodeids.contains(q)) {
                return {eds.rit->first, subgraph_of_kd_core.second};
            }
        }
    } while (eds.addNextShell());

    return {};
}

//////////////////////////////////////////////////////////


void EDS::addShell() {
    for (auto &e : (*rit).second) {
        nodes.insert(e.u);
        nodes.insert(e.v);
        edges.insert(e);
        incidentTimes[e.u].insert({e.t, e.id});
        incidentTimes[e.v].insert({e.t, e.id});
    }
}

void EDS::init(std::map<uint, std::vector<MyTemporalEdge>> const &cores) {
    core_numbers = cores;
    rit = core_numbers.rbegin();
    addShell();
}

bool EDS::addNextShell() {
    ++rit;
    if (rit == core_numbers.rend()) return false;
    addShell();
    return true;
}