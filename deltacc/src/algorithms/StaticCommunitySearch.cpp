#include "StaticCommunitySearch.h"

using namespace std;
using namespace tglib;


std::pair<uint, std::set<NodeId>> compute_static_community(TGS const &tgs, NodeId q) {
    std::map<NodeId, std::vector<MyTemporalEdge>> g;
    std::set<NodeId> all_nodes;

    NodeId mxnid = 0;

    auto edges = tgs.all_edges;

    for (auto &e : edges) {
        g[e.u].push_back(e); // todo assumption graph is undirected
        g[e.v].push_back({e.v, e.u, e.t}); // todo assumption graph is undirected

        if (e.u > mxnid) {
            mxnid = e.u;
        }
        if (e.v > mxnid) {
            mxnid = e.v;
        }

        all_nodes.insert(e.u);
        all_nodes.insert(e.v);
    }

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);

    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid + 1, false);
    std::vector<uint> mindegs;
    std::vector<NodeId> delnodes;

    while (!degrees.empty() && !removed[q] && c[q] > 0) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;

        mindegs.push_back(next->first);
        delnodes.push_back(u);

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});
            }
        }
    }

    uint md = 0;
    uint it = 0;
    for (uint i = 0; i < mindegs.size(); ++i) {
        if (mindegs[i] > md) {
            md = mindegs[i];
            it = i;
        }
    }

    if (it == 0) return {md, all_nodes};

    set<NodeId> total_removed;
    for (int i = 0; i < it-1; ++i) {
        total_removed.insert(delnodes[i]);
    }

    std::set<NodeId> result;
    set_difference(all_nodes.begin(), all_nodes.end(),
                     total_removed.begin(), total_removed.end(), std::inserter(result, result.begin()));




    set<MyTemporalEdge> eds;
    for (auto &e : tgs.all_edges) {
        if (result.contains(e.u) && result.contains(e.v)) {
            eds.insert(e);
            eds.insert(MyTemporalEdge{e.v,e.u,e.t});
        }
    }

    TGS tgsn(mxnid+1, eds);
    auto ndists = tgsn.getDistances(q, result.size());
    result.clear();
    for (int i = 0; i < ndists.size(); ++i) {
        if (ndists[i] != UINT32_MAX) {
            result.insert(i);
        }
    }

    return {md, result};
}


