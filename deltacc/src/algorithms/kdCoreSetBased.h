#ifndef TGDC_KDCORESETBASED_H
#define TGDC_KDCORESETBASED_H

#include "../tg/TGS.h"

std::vector<uint> computeDeltaKCoreSB_original(TGS tgs, tglib::Time delta);

std::map<uint, std::vector<MyTemporalEdge>> cores_to_substreams(TGS const &tgs, std::vector<uint> const &cores);

#endif //TGDC_KDCORESETBASED_H
