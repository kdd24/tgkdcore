#ifndef TGDC_STATICREPRESENTATION_H
#define TGDC_STATICREPRESENTATION_H


#include "TGS.h"

struct SEdge {
    tglib::NodeId u{}, v{};
    tglib::EdgeId e{};
    bool waitingEdge = true;
};

struct SNode{
    tglib::NodeId id, uid;
    tglib::Time t;
    std::vector<SEdge> adjacent;
    bool visited = false;
};

struct SG {
    std::vector<SNode> nodes;
};

SG transformToSG(std::vector<MyTemporalEdge> const &edges, uint delta);

SG transformToSG(TGS const &tgs, uint delta);

#endif //TGDC_STATICREPRESENTATION_H
