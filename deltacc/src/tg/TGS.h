#ifndef TGDC_TGS_H
#define TGDC_TGS_H

#include <cstdint>
#include <tuple>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>

namespace tglib {
    using NodeId = int32_t;
    using EdgeId = int64_t;
    using Time = int64_t;
}

struct MyTemporalEdge {
    tglib::NodeId u;
    tglib::NodeId v;
    tglib::Time t;
    tglib::EdgeId id;

    tglib::NodeId org_u{};
    tglib::NodeId org_u2{};
    tglib::NodeId org_v{};
    tglib::NodeId org_v2{};

};

inline MyTemporalEdge getLowerBoundEdge(tglib::Time t) {
    return MyTemporalEdge{0,0,t,0};
}

inline MyTemporalEdge getUpperBoundEdge(tglib::Time t) {
    return MyTemporalEdge{INT32_MAX,INT32_MAX,t,INT64_MAX};
}

inline bool operator<(const MyTemporalEdge &e1, const MyTemporalEdge &e2) {
    return std::tie(e1.t, e1.u, e1.v, e1.id) < std::tie(e2.t, e2.u, e2.v, e2.id);
}

inline bool operator==(const MyTemporalEdge &e1, const MyTemporalEdge &e2) {
    return e1.u == e2.u && e1.v == e2.v && e1.t == e2.t;
}

inline std::string to_string(const MyTemporalEdge& e) {
    return "(" + std::to_string(e.u) + " " + std::to_string(e.v) + " "
           + std::to_string(e.t) + " " + std::to_string(e.id) + ")";
}

inline std::ostream& operator<<(std::ostream& stream, const MyTemporalEdge& e) {
    stream << to_string(e);
    return stream;
}

std::pair<uint, std::vector<MyTemporalEdge>> reindex_edges(const std::vector<MyTemporalEdge> &edges);

struct TGS {
    TGS(uint numberOfNodes, std::set<MyTemporalEdge> const &edges);

    std::vector<std::set<MyTemporalEdge>> nodes;
    std::vector<MyTemporalEdge> all_edges;
    uint numberOfEdges{};

    [[nodiscard]] uint getNumberOfDeltaNeighbors(tglib::NodeId, tglib::Time t, tglib::Time delta) const;
    [[nodiscard]] std::set<tglib::EdgeId> getDeltaNeighbors(tglib::NodeId, tglib::Time t, tglib::Time delta) const;

    void remove(tglib::EdgeId);

    std::vector<uint> getDistances(tglib::NodeId nid, uint d) const;

    std::unordered_map<tglib::EdgeId, tglib::EdgeId> rev_eidmap;

};

TGS load(std::string const &filename);


#endif //TGDC_TGS_H
