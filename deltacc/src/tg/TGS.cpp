#include "TGS.h"
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;
using namespace tglib;

uint TGS::getNumberOfDeltaNeighbors(tglib::NodeId nid, tglib::Time t, tglib::Time delta) const {
    auto l = nodes[nid].lower_bound(getLowerBoundEdge(max(0l, t - delta)));
    auto u = nodes[nid].upper_bound(getUpperBoundEdge(t + delta )); //+1
    return std::distance(l, u);
}

std::set<tglib::EdgeId> TGS::getDeltaNeighbors(tglib::NodeId nid, tglib::Time t, tglib::Time delta) const {
    auto l = nodes[nid].lower_bound(getLowerBoundEdge(max(0l, t - delta)));
    auto u = nodes[nid].upper_bound(getUpperBoundEdge(t + delta )); //+1
    set<EdgeId> result;
    for (auto it = l; it != u; it++) {
        result.insert((*it).id);
    }
    return result;
}


void TGS::remove(tglib::EdgeId eid) {
    auto const &e = all_edges[eid];
    MyTemporalEdge tge{e.u, e.v, e.t, e.id};
    nodes[e.u].erase(tge);
    nodes[e.v].erase(tge);
}

TGS::TGS(uint numberOfNodes, set<MyTemporalEdge> const &edges) {
    numberOfEdges = edges.size();
    nodes.resize(numberOfNodes);
    EdgeId eid = 0;
    for (MyTemporalEdge e : edges) {
        rev_eidmap[eid] = e.id;
        e.id = eid++;
        all_edges.push_back(e);
        nodes[e.u].insert(e);
        nodes[e.v].insert(e);
    }
}



vector<uint> TGS::getDistances(tglib::NodeId nid, uint d) const {

    set<NodeId> stack;
    vector<uint> distance(nodes.size(), UINT32_MAX);
    distance[nid] = 0;
    stack.insert(nid);

    while (!stack.empty()) {
        NodeId u = *stack.begin();
        stack.erase(u);

        if (distance[u] >= d) continue;
        for (auto &e : nodes[u]) {
            if (e.u == u) {
                if (distance[e.v] == UINT32_MAX) {
                    distance[e.v] = distance[u] + 1;
                    stack.insert(e.v);
                }
            } else if (e.v == u) {
                if (distance[e.u] == UINT32_MAX) {
                    distance[e.u] = distance[u] + 1;
                    stack.insert(e.u);
                }
            }
        }
    }
    return distance;
}



pair<uint, vector<MyTemporalEdge>> reindex_edges(const std::vector<MyTemporalEdge> &edges) {
    tglib::NodeId nid = 0;
    tglib::EdgeId eid = 0;
    std::map<tglib::NodeId, tglib::NodeId> nm;
    std::vector<MyTemporalEdge> result;
    for (MyTemporalEdge const &e : edges) {
        if (nm.find(e.u) == nm.end()) {
            nm[e.u] = nid++;
        }
        if (nm.find(e.v) == nm.end()) {
            nm[e.v] = nid++;
        }

        MyTemporalEdge f{};
        f.id = eid++;
        f.u = nm[e.u];
        f.v = nm[e.v];
        f.t = e.t;
        f.org_u = e.u;
        f.org_v = e.v;
        f.org_u2 = e.org_u;
        f.org_v2 = e.org_v;
        result.push_back(f);
    }
    return {nid, result};
}



struct NodeIdManager {
    NodeIdManager() : nid(0){};
    std::unordered_map<NodeId, NodeId> nidmap;
    NodeId getNodeId(NodeId id) {
        NodeId r;
        if (nidmap.find(id) == nidmap.end()) {
            r = nid;
            nidmap.emplace(id, nid++);
        } else {
            r = nidmap.at(id);
        }
        return r;
    }
    NodeId nid = 0;
};


std::vector<Time> split_string1(const std::string& s, char delim) {
    std::vector<Time> result;
    std::vector<string> strings;
    std::stringstream ss(s);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, delim);
        if (!substr.empty()) {
            try {
                stol(substr);
            }
            catch(...) {
                continue;
            }
            result.push_back(stol(substr));
        }
    }
    return result;
}

TGS load(string const &filename) {

    std::ifstream fs;
    fs.open(filename);
    auto opened_file = fs.is_open();

    if (!opened_file) {
        throw std::runtime_error("Could not open file " + filename);
    }

    std::ifstream fs2;

    std::string line;

    std::set<MyTemporalEdge> edges;
    std::set<Time> times;

    NodeIdManager nidman;
    while (getline(fs, line)) {

        if (line.empty()) {
            continue;
        }
        auto l = split_string1(line, ' ');

        if (l.size() < 3 || l[0] == l[1]) {
            continue;
        }

        Time t = l[2];

        times.insert(t);

        NodeId u = nidman.getNodeId(l[0]);
        NodeId v = nidman.getNodeId(l[1]);

        if (u > v) swap(u, v);

        edges.insert(MyTemporalEdge{u, v, t, 0});
    }
    fs.close();

    set<MyTemporalEdge> result;
    EdgeId eid = 0;
    for (auto e : edges) {
        e.id = eid++;
        result.insert(e);
    }

    cout << "Loades tgs from file " << filename << endl;
    cout << "#nodes: " << nidman.nid << endl;
    cout << "#edges: " << result.size() << endl;
    cout << "#times: " << times.size() << endl;
    cout << "time interval: [" << *times.begin() << "," << *times.rbegin() << "]  |T|="
         << *times.rbegin() - *times.begin() + 1 << endl;

    TGS tgs(nidman.nid, edges);
    return tgs;
}


