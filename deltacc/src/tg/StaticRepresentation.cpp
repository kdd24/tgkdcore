#include "StaticRepresentation.h"
#include "TGS.h"

using namespace std;
using namespace tglib;


SG transformToSG(vector<MyTemporalEdge> const &edges, uint delta) {
    map<NodeId, pair<NodeId, Time>> time_nodes;
    map<pair<NodeId, Time>, NodeId> rm;
    NodeId nid = 0;
    SG sg;

    for (auto &e : edges) {
        if (!rm.contains({e.u, e.t}))
            rm[{e.u, e.t}] = nid++;
        if (!rm.contains({e.v, e.t}))
            rm[{e.v, e.t}] = nid++;
    }
    for (auto &p : rm) {
        sg.nodes.push_back(SNode{p.second, p.first.first, p.first.second, vector<SEdge>()});
    }
    sort(sg.nodes.begin(), sg.nodes.end(), [](auto const &a, auto const &b){return a.id < b.id;});
    auto it1 = rm.begin();
    auto it2 = it1;
    ++it2;
    while (it1 != rm.end() && it2 != rm.end()) {
        if (it1->first.first == it2->first.first) {
            if (abs(it1->first.second - it2->first.second) <= delta) {
                sg.nodes[it1->second].adjacent.push_back(SEdge{it1->second,it2->second,0});
                sg.nodes[it2->second].adjacent.push_back(SEdge{it2->second,it1->second,0});
            }
        }
        ++it1;
        ++it2;
    }
    for (EdgeId eid = 0; eid < edges.size(); ++eid) {
        auto e = edges[eid];
        NodeId n1 = rm[{e.u, e.t}];
        NodeId n2 = rm[{e.v, e.t}];
        sg.nodes[n1].adjacent.push_back(SEdge{n1, n2, eid, false});
        sg.nodes[n2].adjacent.push_back(SEdge{n2, n1, eid, false});
    }

    return sg;
}

SG transformToSG(TGS const &tgs, uint delta) {
    return transformToSG(tgs.all_edges, delta);
}